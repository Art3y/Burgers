import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://burger160299.firebaseio.com/'
});

export default instance;